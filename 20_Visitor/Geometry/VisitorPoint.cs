﻿using _20_Visitor.SerializatorStrategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.Geometry
{
    class VisitorPoint : IGeometryVisitor
    {
        public bool Visit(StrategyJSONSerialization strategy)
        {
            return true;
        }

        public bool Visit(StrategyXMLSerialization strategy)
        {
            return true;
        }
    }
}
