﻿using _20_Visitor.SerializatorStrategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.Geometry
{
    interface IGeometryVisitor
    {
        bool Visit(StrategyJSONSerialization strategy);
        bool Visit(StrategyXMLSerialization strategy);
    }
}
