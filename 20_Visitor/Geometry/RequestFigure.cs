﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.Geometry
{    
    public class RequestFigure
    {
        
        public  string visitor;
        public  string serialization;       
        public  int posX;        
        public  int posY;        
        public  int radius;        
        public  int height;
        public  int widht;
        public RequestFigure()
        {

        }

        public RequestFigure(string visitor, string serialization, int posX, int posY,
            int radius = 0,int height = 0,int widht=0)
        {
            this.visitor = visitor;
            this.serialization = serialization;
            this.posX = posX;
            this.posY = posY;
            this.radius = radius;
            this.height = height;
            this.widht = widht;
        }
    }

    
}
