﻿using _20_Visitor.SerializatorStrategy;
using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.Geometry
{
    class VisitorCircle : IGeometryVisitor
    {  
        public bool Visit(StrategyJSONSerialization strategy)
        {
            return true;
        }

        public bool Visit(StrategyXMLSerialization strategy)
        {
            return true;
        }
    }
}
