﻿using _20_Visitor.Geometry;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace _20_Visitor.SerializatorStrategy
{
    class StrategyJSONSerialization : IGeometryStrategy
    {
        public string Execute(IGeometryVisitor visitor, RequestFigure geometryFigure)
        {
            if (!visitor.Visit(this)) return "JSON Serialization is unenable";
            return JsonConvert.SerializeObject(geometryFigure);
            //return JsonSerializer.Serialize(geometryFigure);
        }
    }
}
