﻿using _20_Visitor.Geometry;
using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.SerializatorStrategy
{
    interface IGeometryStrategy
    {
        string Execute(IGeometryVisitor visitor, RequestFigure geometryFigure);
    }
}
