﻿using _20_Visitor.Geometry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace _20_Visitor.SerializatorStrategy
{
    class StrategyXMLSerialization : IGeometryStrategy
    {
        public string Execute(IGeometryVisitor visitor, RequestFigure geometryFigure)
        {
            if (!visitor.Visit(this)) return "XML Serialization is unenable";
            XmlSerializer xs = new XmlSerializer(typeof(RequestFigure));
            using(Stream s=File.Create(geometryFigure.visitor.ToString()) )
            {
                xs.Serialize(s, geometryFigure);
            }
            
            using (Stream s =File.OpenRead(geometryFigure.visitor.ToString()))
            {
                // преобразуем строку в байты
                byte[] array = new byte[s.Length];
                // считываем данные
                s.Read(array, 0, array.Length);
                // декодируем байты в строку
                return  System.Text.Encoding.Default.GetString(array);                
            }    
        }
    }
}
