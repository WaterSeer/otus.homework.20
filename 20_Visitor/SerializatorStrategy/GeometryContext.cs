﻿using _20_Visitor.Geometry;
using System;
using System.Collections.Generic;
using System.Text;

namespace _20_Visitor.SerializatorStrategy
{
    class GeometryContext
    {
        IGeometryStrategy strategy;
        IGeometryVisitor visitor;       

        public string Request(RequestFigure request)
        {
            SetStrategy(request);
            SetVisitor(request);
            return strategy.Execute(visitor, request);
        }

        private void SetStrategy(RequestFigure request)
        {
            var x = Activator.CreateInstance(null,
                "_20_Visitor.SerializatorStrategy.Strategy" + request.serialization);
            strategy = (IGeometryStrategy)x.Unwrap();
        }

        private void SetVisitor(RequestFigure request)
        {
            var x = Activator.CreateInstance(null,
                "_20_Visitor.Geometry.Visitor" + request.visitor);
            visitor = (IGeometryVisitor)x.Unwrap();
        }         
    }
}
