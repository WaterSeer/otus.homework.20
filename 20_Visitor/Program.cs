﻿
using _20_Visitor.Geometry;
using _20_Visitor.SerializatorStrategy;
using System;

namespace _20_Visitor
{
    class Program
    {
        static void Main(string[] args)
        {
            GeometryContext context = new GeometryContext();
            RequestFigure PointJSON = new RequestFigure("Point", "JSONSerialization", 3, 4);
            RequestFigure PointXML = new RequestFigure("Point", "XMLSerialization", 16, 34);
            Console.WriteLine(context.Request(PointJSON));
            Console.WriteLine();
            Console.WriteLine(context.Request(PointXML));
            Console.WriteLine();

            RequestFigure CircleJSON = new RequestFigure("Circle", "JSONSerialization", 16, 5, 13);
            RequestFigure SquareXML = new RequestFigure("Square", "XMLSerialization", 34, 5, 0, 12, 12);
            Console.WriteLine(context.Request(CircleJSON));
            Console.WriteLine();
            Console.WriteLine(context.Request(SquareXML));
            Console.WriteLine();

            Console.ReadKey();
        }
    }
}
